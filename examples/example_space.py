####### PACKAGES
from cytolysis import cytosim_analysis as ana

# Filenames
import pkg_resources as pk
space_file = pk.resource_filename( "cytolysis" , 'example_data/space.txt')
config_file = pk.resource_filename( "cytolysis" , 'example_data/example.cym')

# Simple example functions for analysis
def space_shape(frame):
    return frame.objects["space"]["cell"].properties["shape"]

from cytolysis.objects import Object
def read_space(space_set, reports=None, *args, **kwargs):
    try:
        if "state" in reports.keys():
            for line in reports["state"]:
                line = line.strip().strip("\n")
                if line and line.find("%")<0 and not line.isspace():
                    space_set.append(Object(*args, id=line.split()[1], **kwargs))
    except:
        print("Could not read space")


# Reading from report and config files
space_reports = {"state": space_file}
space_options = {"read": read_space}
# Creating simulation representation
simul = ana.Simulation(space_report={"cell": space_reports }, space_options=space_options,
                       config=config_file)

# Reading properties in object simul
print(" Simulation properties : %s" %simul.properties)
print(" Cell properties : %s" % simul[0].objects["space"]["cell"].properties)
print(" Number of frames : %s" % len(simul))

# Declaration of analysis functions
analyzer={"frame": {"shape": space_shape}}

# Performing analysis
simul.make_analysis(analyzer=analyzer)
ana.export_analysis(simul.frames_analysis, "frames_space.csv")
