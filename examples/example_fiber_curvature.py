from cytolysis import cytosim_analysis as ana

# Filenames
import pkg_resources as pk
fibers_file = pk.resource_filename( "cytolysis" , 'example_data/fiber_points.txt')
config_file = pk.resource_filename( "cytolysis" , 'example_data/example.cym')

# Simple example functions for analysis
def sum_curvature_energies(frame):
    return frame.analysis["microtubule"]['deformation_energy'].sum()

def mean_curvature_energy(simul):
    return simul.frames_analysis['sum_energy'].mean()

# Reading from report and config files
microtubule_reports={"points" : fibers_file}

# Creating simulation representation
simul = ana.Simulation(fibers_report={"microtubule": microtubule_reports},
                       config=config_file)

# Reading properties in object simul
print(" Microtubule properties : %s" % simul[0].objects["fiber"]['microtubule'].properties )

# Declaration of analysis functions
analyzer={ 'frame' : { 'sum_energy': sum_curvature_energies},
           'simulation' : {'mean_energy' : mean_curvature_energy}}

# Performing analysis
simul.make_analysis(analyzer=analyzer)
print(" Mean curvature energy : %s" % simul.analysis['mean_energy'][0])
# Saving to file
ana.export_analysis(simul.frames_analysis, 'frames_energy.csv')
ana.export_analysis(simul.analysis, 'simul_energy.csv')
