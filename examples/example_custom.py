####### PACKAGES
from cytolysis import cytosim_analysis as ana
import numpy as np
# Filenames
import pkg_resources as pk
solids_file = pk.resource_filename( "cytolysis" , 'example_data/solids.txt')
config_file = pk.resource_filename( "cytolysis" , 'example_data/example.cym')

# Here we we just need to tell Object_set how to create solids from the report file

from cytolysis.objects import Object
def solid_reader(solid_set, reports=None ):
    try:
        rows = np.loadtxt(reports['position'],comments="%")
        for row in rows:
            solid_set.append(Object(position=row[2:5], id=row[1]))
    except:
        print("Could not read solids")

def intercore_distance(frame):
    return np.linalg.norm(frame.objects["solid"]["core"][0].position
                          - frame.objects["solid"]["core"][1].position)

# Reading from report and config files
solid_reports = {"position": solids_file}

# In the options, we need to specify what is the type of the object, and usually how to read the report files
options = {"type" : "solid" , "read" : solid_reader}

# Creating simulation representation
simul = ana.Simulation(solid_report={"core" : solid_reports },
                       solid_options=options,
                       config=config_file)
# Reading properties in object simul
print(" Simulation properties : %s" %simul.properties)
print(" Core properties : %s" % simul[0].objects["solid"]["core"].properties)
print(" Number of frames : %s" %len(simul))

# Declaration of analysis functions
analyzer = {'frame' : {'distance': intercore_distance } }

# Performing analysis
simul.make_analysis(analyzer=analyzer)
ana.export_analysis(simul.frames_analysis, 'frames_solid.csv')
