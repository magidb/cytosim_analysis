Module cytolysis.read_config
============================
Read cytosim configuration files

Syntax:
   
    read_config.py FILE
    read_config.py - FILE
    
Description:

    Reads a config file and prints a formatted copy to standard output.
    Two formats are possible: vertical (default) or horizontal (second syntax)

F. Nedelec 11.2013--20.02.2017

Functions
---------

    
`convert(arg)`
:   Convert a string to a int or float if possible

    
`delimiter(c)`
:   

    
`file_object(arg)`
:   

    
`format_config(pile, mode=0, prefix='')`
:   Returns a compact representation of the hierachical dictionnary

    
`format_values(val)`
:   Convert values to a cannonical ASCII representation

    
`get_block(fid, s, e)`
:   Return a block including the enclosing delimiters

    
`get_char(fid)`
:   

    
`get_command(pile, keys)`
:   Return all commands corresponding to = [ command, class, name ]
    The keys can be '*' to specify a wildcard matching

    
`get_hexadecimal(fid, s)`
:   

    
`get_number(fid, s)`
:   Read a number with decimal point and optional exponent

    
`get_token(fid)`
:   Extract the next token from the file

    
`get_until(fid, e)`
:   

    
`get_value(arg, keys)`
:   Return the value speficied by keys = [ command, class, name, parameter ]
    The first 3 keys can be '*' for wildcard matching

    
`main(args)`
:   

    
`parse(arg)`
:   Returns a dictionnary obtained by parsing the file specified by name `arg`

    
`parse_config(fid)`
:   return the list resulting from parsing the specified file

    
`read_list(fid)`
:   Process a list of key=values

    
`simplify(arg)`
:   Simplify the values of imbricated sets and dictionaries

    
`uncode(arg)`
:   

    
`valid_token_char(c)`
:   

Classes
-------

`Instruction(key)`
:   

    ### Class variables

    `cnt`
    :

    `vals`
    :

    `words`
    :

    ### Methods

    `format(self, mode)`
    :

    `format_header(self)`
    :

    `format_horizontal(self)`
    :

    `format_vertical(self)`
    :

    `value(self, key, index=0)`
    :

    `values(self)`
    :