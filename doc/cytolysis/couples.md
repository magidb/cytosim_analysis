Module cytolysis.couples
========================

Classes
-------

`Couple(*args, id=1, fiber1=None, fiber2=None, hand1=None, hand2=None, position=None, state=None, **kwargs)`
:   Couple
    A class that contains a couple. Yep, a whole class for that.

`Couple_set(*args, id=1, config=None, hand1_props=None, hand2_props=None, type='couples', name='couple', build=None, **kwargs)`
:   Couple_set
    A class that contains a list of couples plus extra methods and properties

    ### Ancestors (in MRO)

    * cytolysis.objects.Object_set
    * builtins.list

    ### Methods

    `build_couples(self, *args, repoints=None, name=None, **kwargs)`
    :   Building from numpy array read in report file

    `make_couples_from_state_lines(self, *args, points=None, **kwargs)`
    :   helper function