Module cytolysis.cytosim_analysis
=================================

Classes
-------

`Frame(*args, number=None, blocks=None, **kwargs)`
:   A frame is a data structure containing several objects: Fibers, Couples, Space, etc.
    
        frame.objects is a dictionary index by types, of dictionaries indexed by names, of objecs.
        e.g. frame.objects["fiber"]["microtubule"] = list_MT
        where list_MT = [MT1, MT2, ... ], with MT1, MT2, ... being instances of class Fiber

    ### Methods

    `hide(self)`
    :   Hides a frame from display

    `initialize(self, *args, blocks=None, config=None, options=None, dim=3, **kwargs)`
    :   Actually create the objects inside the frame

    `make_analysis(self, *args, **kwargs)`
    :   A wrapper for object analysis

    `plot(self, *args, options=None, **kwargs)`
    :   A wrapper for object plotting

    `show(self)`
    :   Shows a frame in display

`Simulation(*args, dim=3, **kwargs)`
:   Simulation is the main class
        it is a list of frames  initiated with reports and config files,
        eg : config=config.cym, fibers_reports={ "microtubule" : { "points" : "fiber_points.txt" } }
        ...

    ### Ancestors (in MRO)

    * builtins.list

    ### Methods

    `analyze(self, frame, *args, analyzer=None, **kwargs)`
    :   Analysis per frame

    `figure(self)`
    :   Whatever that does

    `frame_player(self, interval=2000, *args, **kwargs)`
    :   Shows the system frame by frame

    `initialize(self, *args, config=None, options=None, **kwargs)`
    :   We actually build the simulation here
        We will make the frames from the report files using iterators over these files
        
        iters is a dictionary of dictionary of dictionaries of iterators :
        e.g.
        iters["fiber] = { "microtubule" : { "points" : iterator_to_fiber_points } }

    `make_analysis(self, *args, analyzer=None, **kwargs)`
    :   Analyzes the whole simulation (simulation, frames, and objects)

    `plot(self, number, *args, **kwargs)`
    :   Plots a frame number

    `plot_all(self, *args, **kwargs)`
    :   Plots all frames

    `read_config(self, config, simul_props=None, **kwargs)`
    :

    `show(self, *args, **kwargs)`
    :   Show the plotted system

    `show_frame(self, number, *args, **kwargs)`
    :   shows a specific frame