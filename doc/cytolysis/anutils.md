Module cytolysis.anutils
========================

Functions
---------

    
`concatenate_dicts(*args)`
:   Dict concaternation

    
`export_analysis(analysis, fname='analysis.csv')`
:   Exports a dataframe to a csv file

    
`get_block(iterator, key)`
:   

    
`get_dict_from_pile(pile, com)`
:   Getting a dict from the config represented as a pile

    
`get_frame_block(iterator)`
:   

    
`get_options(options, name=None, type=None)`
:   Reads the options for an object of given type and name
       first looks for options for the type, then for the name
       i.e. specialized options superseeds general ones

    
`get_prop_dicts(pile, key='set', type=None, name=None)`
:   Getting a dictionary of properties

    
`make_analysis(dict, *args, **kwargs)`
:   iterates over a dict of object to perform analysis

    
`make_block_dict(dict_dict)`
:   

    
`make_iter_dict(dict_dict)`
:   

    
`objects_analysis(objects, analyzer=None, *args, **kwargs)`
:   Iterate over an object set to analyze its objects
    technically could work for iterators ;)